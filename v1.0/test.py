# import socket, uuid, re, binascii
# from binascii import hexlify
# import netifaces as nif
# # the public network interface
# HOST = socket.gethostbyname(socket.gethostname())

# create a raw socket and bind it to the public interface
# s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_IP)
# s.bind((HOST, 80))

# # Include IP headers
# s.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)

# # receive all packages
# s.ioctl(socket.SIO_RCVALL, socket.RCVALL_ON)

# # receive a package
# print(s.recvfrom(65565))

# # disabled promiscuous mode
# s.ioctl(socket.SIO_RCVALL, socket.RCVALL_OFF)

# mac =  hexlify(s.getsockname()[4])
# p = socket.socket(socket.AF_INET, socket.SOCK_RAW)

# mac = ':'.join(re.findall('..', '%012x' % uuid.getnode()))
# mac2 = uuid.getnode()

# print(mac)
# print(mac2)
# p.bind((mac, 0))

# print(p.getsockname())


import socket as s

HOSTS = ('169.254.222.66', '10.50.220.132')
for HOST in HOSTS:
    print('---', HOST)
    socket = s.socket(s.AF_INET, s.SOCK_RAW)
    socket.bind(('localhost', 0))
    socket.ioctl(s.SIO_RCVALL, s.RCVALL_ON)
    for i in range(10):
        x = socket.recvfrom(65535)
        print(x[1], ' '.join(['%02x' % b for b in x[0]][:20]))
    socket.ioctl(s.SIO_RCVALL, s.RCVALL_OFF)